package br.com.ozcorp;

/**
 * 
 * @author Laisa Nic�cio
 *
 */

public class OzcorpTeste {

	public static void main(String[] args) {
		
		Engenheiro engEletrico = new Engenheiro("Laisa", "65897254", "65687242", "laisa@email.com", TiposFuncionarios.ENGENHEIRO, TipoSanguineo.ABNEGATIVO, "65faldakld", "48584a5d89a4", Sexo.FEMININO, new Cargo("Engenhreiro El�trico", 10000000), NivelAcesso.ENGENHEIRO);
		
		System.out.println("Nome: "+ engEletrico.getNome());
		System.out.println("RG: "+engEletrico.getRg());
		System.out.println("CPF: "+engEletrico.getCpf());
		System.out.println("Nivel de Acesso: "+engEletrico.getNivelAcesso().nivelAcesso);
		System.out.println("Matricula: "+engEletrico.getMatricula());
		System.out.println("E-mail: "+engEletrico.getEmail());
		System.out.println("Senha: "+engEletrico.getSenha());
		System.out.println("Cargo: "+engEletrico.getCargo().getTitulo());
		System.out.println("Sal�rio Base: "+engEletrico.getCargo().getSalarioBase());
		System.out.println("Tipo Sanguineo: "+engEletrico.getTipoSanguineo().sangue);
		System.out.println("Sexo: "+engEletrico.getSexo().s);
		
		System.out.println();
		
		Analista anFinaceiro = new Analista("Camila", "89721456", "65872542", "camila@email.com", TiposFuncionarios.ANALISTA, TipoSanguineo.APOSITIVO, "sldkjakldj2547", "LJKSU47844", Sexo.FEMININO, new Cargo("Analista Financeiro", 68900000), NivelAcesso.ANALISTA);
		
		System.out.println("Nome: "+ anFinaceiro.getNome());
		System.out.println("RG: "+anFinaceiro.getRg());
		System.out.println("CPF: "+anFinaceiro.getCpf());
		System.out.println("Nivel de Acesso: "+anFinaceiro.getNivelAcesso().nivelAcesso);
		System.out.println("Matricula: "+anFinaceiro.getMatricula());
		System.out.println("E-mail: "+anFinaceiro.getEmail());
		System.out.println("Senha: "+anFinaceiro.getSenha());
		System.out.println("Cargo: "+anFinaceiro.getCargo().getTitulo());
		System.out.println("Sal�rio Base: "+anFinaceiro.getCargo().getSalarioBase());
		System.out.println("Tipo Sanguineo: "+anFinaceiro.getTipoSanguineo().sangue);
		System.out.println("Sexo: "+anFinaceiro.getSexo().s);
		
		System.out.println();
		
		Secretario sFinanceiro = new Secretario("Jo�o", "65898754", "123467887", "joao@email.com", TiposFuncionarios.SECRETARIO, TipoSanguineo.BNEGATIVO, "123!", "659785424", Sexo.MASCULINO, new Cargo("Secret�rio Financeiro", 560000000), NivelAcesso.SECRETARIO);
		
		System.out.println("Nome: "+ sFinanceiro.getNome());
		System.out.println("RG: "+sFinanceiro.getRg());
		System.out.println("CPF: "+sFinanceiro.getCpf());
		System.out.println("Nivel de Acesso: "+sFinanceiro.getNivelAcesso().nivelAcesso);
		System.out.println("Matricula: "+sFinanceiro.getMatricula());
		System.out.println("E-mail: "+sFinanceiro.getEmail());
		System.out.println("Senha: "+sFinanceiro.getSenha());
		System.out.println("Cargo: "+sFinanceiro.getCargo().getTitulo());
		System.out.println("Sal�rio Base: "+sFinanceiro.getCargo().getSalarioBase());
		System.out.println("Tipo Sanguineo: "+sFinanceiro.getTipoSanguineo().sangue);
		System.out.println("Sexo: "+sFinanceiro.getSexo().s);
		
		System.out.println();
		
		Diretor dirExecutivo = new Diretor("Marcelo", "25598784", "46658974", "marcelo@email.com", TiposFuncionarios.DIRETOR, TipoSanguineo.ONEGATIVO, "6565fafa", "ASD565SDA", Sexo.OUTRO, new Cargo("Diretor Executivo", 865000000), NivelAcesso.DIRETOR);
		
		System.out.println("Nome: "+ dirExecutivo.getNome());
		System.out.println("RG: "+dirExecutivo.getRg());
		System.out.println("CPF: "+dirExecutivo.getCpf());
		System.out.println("Nivel de Acesso: "+dirExecutivo.getNivelAcesso().nivelAcesso);
		System.out.println("Matricula: "+dirExecutivo.getMatricula());
		System.out.println("E-mail: "+dirExecutivo.getEmail());
		System.out.println("Senha: "+dirExecutivo.getSenha());
		System.out.println("Cargo: "+dirExecutivo.getCargo().getTitulo());
		System.out.println("Sal�rio Base: "+dirExecutivo.getCargo().getSalarioBase());
		System.out.println("Tipo Sanguineo: "+dirExecutivo.getTipoSanguineo().sangue);
		System.out.println("Sexo: "+dirExecutivo.getSexo().s);
		
		System.out.println();
		
		Gerente geRH = new Gerente("Lana", "65988744", "6897031", "lana@email.com", TiposFuncionarios.GERENTE, TipoSanguineo.ABPOSITIVO, "65987jdgaj", "635A6587SA", Sexo.FEMININO, new Cargo("Gerente de RH", 123300000), NivelAcesso.GERENTE);
		
		System.out.println("Nome: "+ geRH.getNome());
		System.out.println("RG: "+geRH.getRg());
		System.out.println("CPF: "+geRH.getCpf());
		System.out.println("Nivel de Acesso: "+geRH.getNivelAcesso().nivelAcesso);
		System.out.println("Matricula: "+geRH.getMatricula());
		System.out.println("E-mail: "+geRH.getEmail());
		System.out.println("Senha: "+geRH.getSenha());
		System.out.println("Cargo: "+geRH.getCargo().getTitulo());
		System.out.println("Sal�rio Base: "+geRH.getCargo().getSalarioBase());
		System.out.println("Tipo Sanguineo: "+geRH.getTipoSanguineo().sangue);
		System.out.println("Sexo: "+geRH.getSexo().s);
		System.out.println("Nivel de Acesso: "+geRH.getNivelAcesso());
	}
}
